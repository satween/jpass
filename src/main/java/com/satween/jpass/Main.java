package com.satween.jpass;

import com.satween.jpass.commands.Check;
import com.satween.jpass.commands.Command;
import com.satween.jpass.commands.Encrypt;
import org.jasypt.util.password.BasicPasswordEncryptor;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Main {
    private static final String ENCRYPT_COMMAND = "encrypt";
    private static final String CHECK_COMMAND = "check";
    private static final String HELP_MESSAGE = "Usage: command <arg1> <arg2>\n" +
            "\t check <password> <encryption>\n" +
            "\t encrypt <password>";

    private static Map<String, Command> controlMap;

    static {
        controlMap = new HashMap<String, Command>();
        controlMap.put(ENCRYPT_COMMAND, new Encrypt(new BasicPasswordEncryptor()));
        controlMap.put(CHECK_COMMAND, new Check(new BasicPasswordEncryptor()));
    }


    public static void main(String[] args) throws InterruptedException {
        if (args.length < 2) {
            help("Invalid arguments");
            return;
        }

        String command = args[0];
        if (!controlMap.containsKey(command)) {
            help(String.format("Unknown command %s", command));
            return;
        }

        output(controlMap.get(command).exec(Arrays.copyOfRange(args, 1, args.length)));

    }

    private static void help(String message) {
        output(String.format("%s\n%s", message, HELP_MESSAGE));


    }

    private static void output(String message) {
        System.out.println(message);
    }
}
