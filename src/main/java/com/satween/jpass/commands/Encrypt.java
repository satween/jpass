package com.satween.jpass.commands;

import org.jasypt.util.password.PasswordEncryptor;

public class Encrypt implements Command {
    private final PasswordEncryptor encryptor;

    public Encrypt(PasswordEncryptor encryptor) {
        this.encryptor = encryptor;
    }

    public String exec(String[] args) {
        final String password = args[0];
        return String.format("Encryption of %s is %s", password, encryptor.encryptPassword(password));
    }
}
