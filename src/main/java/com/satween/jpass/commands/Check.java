package com.satween.jpass.commands;

import org.jasypt.util.password.PasswordEncryptor;

public class Check implements Command {
    private final PasswordEncryptor encryptor;

    public Check(PasswordEncryptor encryptor) {
        this.encryptor = encryptor;
    }

    public String exec(String[] args) {
        return String.format("Checking result is: %s",
                String.valueOf(encryptor.checkPassword(args[0], args[1]))
                        .toUpperCase());

    }
}
