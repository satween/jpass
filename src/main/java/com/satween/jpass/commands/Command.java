package com.satween.jpass.commands;

public interface Command {
    String exec(String[] args);
}
