# README #

This is a client to encrypt/check passwords with jsypt library

### Usage ###
* To encrypt password:

```
#!java

$ java -jar jpass.jar encrypt password
Encryption of password is GhUn99f0EWXDjoBrn4N4ea80FRl0mUrL
```
* To check encrypted password

```
#!java

$ java -jar jpass.jar check password GhUn99f0EWXDjoBrn4N4ea80FRl0mUrL
Checking result is: TRUE

```